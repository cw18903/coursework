/*
** Code to implement a d2q9-bgk lattice boltzmann scheme.
** 'd2' inidates a 2-dimensional grid, and
** 'q9' indicates 9 velocities per grid cell.
** 'bgk' refers to the Bhatnagar-Gross-Krook collision step.
**
** The 'speeds' in each cell are numbered as follows:
**
** 6 2 5
**  \|/
** 3-0-1
**  /|\
** 7 4 8
**
** A 2D grid:
**
**           cols
**       --- --- ---
**      | D | E | F |
** rows  --- --- ---
**      | A | B | C |
**       --- --- ---
**
** 'unwrapped' in row major order to give a 1D array:
**
**  --- --- --- --- --- ---
** | A | B | C | D | E | F |
**  --- --- --- --- --- ---
**
** Grid indicies are:
**
**          ny
**          ^       cols(ii)
**          |  ----- ----- -----
**          | | ... | ... | etc |
**          |  ----- ----- -----
** rows(jj) | | 1,0 | 1,1 | 1,2 |
**          |  ----- ----- -----
**          | | 0,0 | 0,1 | 0,2 |
**          |  ----- ----- -----
**          ----------------------> nx
**
** Note the names of the input parameter and obstacle files
** are passed on the command line, e.g.:
**
**   ./d2q9-bgk input.params obstacles.dat
**
** Be sure to adjust the grid dimensions in the parameter file
** if you choose a different obstacle file.
*/
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

#define NSPEEDS         9
#define FINALSTATEFILE  "final_state.dat"
#define AVVELSFILE      "av_vels.dat"
#define MASTER          0

/* struct to hold the parameter values */
typedef struct
{
  int   nx;            /* no. of cells in x-direction */
  int   ny;            /* no. of cells in y-direction */
  int   maxIters;      /* no. of iterations */
  int   reynolds_dim;  /* dimension for Reynolds number */
  int   nya;           /* no. of cells in y-direction in local array */
  int   offset;        /* offset to use in main grid for current rank */
  int   nextoffset;    /* offset to use in main grid for next rank */
  int   arraysize;     /* size of main grid for current rank */
  int   remainder;     /* remainder of params->nx / nprocs */
  int   rank;          /* rank of this mpi thread */
  int   nextrank;      /* rank of the next mpi thread */
  int   prevrank;      /* rank of the last mpi thread */
  int   nprocs;        /* number of mpi threads */
  int   tot_cells;     /* number of non obstacle cells */
  float density;       /* density per link */
  float accel;         /* density redistribution */
  float omega;         /* relaxation parameter */
} t_param;

/* struct to hold the 'speed' values */
typedef struct
{
  float * restrict speeds0;
  float * restrict speeds1;
  float * restrict speeds2;
  float * restrict speeds3;
  float * restrict speeds4;
  float * restrict speeds5;
  float * restrict speeds6;
  float * restrict speeds7;
  float * restrict speeds8;
} t_speed;

/*
** function prototypes
*/

/* load params, allocate memory, load obstacles & initialise fluid particle densities */
int initialise(const char* paramfile, const char* obstaclefile,
               t_param* params, t_speed* cells_ptr, t_speed* tmp_cells_ptr, t_speed* fnl_cells_ptr,
               char** obstacles_ptr, char** all_obstacles_ptr, float** av_vels_ptr, float** fnl_av_vels_ptr, float** sendbuf, float** recvbuf, int** sizes_ptr, int** displs_ptr, const int rank, const int nprocs);

/*
** The main calculation methods.
** timestep calls, in order, the functions:
** accelerate_flow(), propagate(), rebound() & collision()
*/
int accelerate_flow(const t_param* params_ptr, t_speed* cells_ptr, char* restrict obstacles_ptr);
int haloex(const t_param* params_ptr, t_speed* cells_ptr, float* sendbuf, float* recvbuf, MPI_Status* status);
float rebound(const t_param* params, const t_speed* cells, t_speed* tmp_cells, const char* restrict obstacles);
int write_values(const t_param* params_ptr, const t_speed* cells_ptr, const char* obstacles_ptr, const float* av_vels_ptr);

/* finalise, including freeing up allocated memory */
int finalise(const t_param* params, t_speed* cells_ptr, t_speed* tmp_cells_ptr, t_speed* fnl_cells_ptr,
             char** obstacles_ptr, char** all_obstacles_ptr, float** av_vels_ptr, float** fnl_av_vels_ptr, float** sendbuf, float** recvbuf, int** sizes_ptr, int** displs_ptr);

/* Sum all the densities in the grid.
** The total should remain constant from one timestep to the next. */
float total_density(const t_param* params_ptr, const t_speed* cells_ptr);

/* utility functions */
void die(const char* message, const int line, const char* file);
void usage(const char* exe);

/*
** main program:
** initialise, timestep loop, finalise
*/
int main(int argc, char* argv[])
{
  char*    paramfile    = NULL;    /* name of the input parameter file */
  char*    obstaclefile = NULL;    /* name of a the input obstacle file */
  char*    obstacles    = NULL;    /* local grid indicating which cells are blocked */
  char*    allobstacles = NULL;    /* grid indicating which cells are blocked */
  int*     sizes        = NULL;    /* array to hold the sizes of all grids */
  int*     displs       = NULL;    /* array to hold the offsets of all grids */
  float*   sendbuf      = NULL;    /* buffer for sending halo */
  float*   recvbuf      = NULL;    /* buffer for recieving halo */
  float*   fnl_av_vels  = NULL;    /* a record of the av. velocity computed for each timestep - might need to be double */
  float*   av_vels      = NULL;    /* a record of the av. velocity computed for each timestep - might need to be double */
  float    partialdensity;         /* variable to hold partial densities for each rank */
  float    totaldensity;           /* variable to reduce all partial densities into */
  int      tot_cells;              /* no. of cells used in calculation */
  int      nprocs, rank, flag;     /* Mpi variables */
  t_param  params;                 /* struct to hold parameter values */
  t_speed  cells;                  /* grid containing fluid densities */
  t_speed  tmp_cells;              /* scratch space */
  t_speed  fnl_cells;              /* grid containing final state */
  t_speed  intermediate;           /* grid for pointer swap */
  double   tot_tic, tot_toc, init_tic, init_toc, comp_tic, comp_toc, col_tic, col_toc; /* floating point numbers to calculate elapsed wallclock time */
  MPI_Status status;               /* struct used by MPI */
  struct timeval timstr;           /* structure to hold elapsed time */
  struct rusage  ru;               /* structure to hold CPU time--system and user */
  enum bool {FALSE,TRUE};          /* enumerated type: false = 0, true = 1 */

  /* initialise our MPI environment */
  MPI_Init(&argc, &argv);

  /* check whether the initialisation was successful */
  MPI_Initialized(&flag);
  if (!flag) {
  	MPI_Abort(MPI_COMM_WORLD,EXIT_FAILURE);
  }

  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  /* parse the command line */
  if (argc != 3)
  {
    usage(argv[0]);
  }
  else
  {
    paramfile = argv[1];
    obstaclefile = argv[2];
  }

  if (rank == MASTER) {
    /* Total/init time starts here: initialise our data structures and load values from file */
    gettimeofday(&timstr, NULL);
    tot_tic = timstr.tv_sec + (timstr.tv_usec / 1000000.0);
    init_tic = tot_tic;
  }

  /* initialise our data structures and load values from file */
  initialise(paramfile, obstaclefile, &params, &cells, &tmp_cells, &fnl_cells, &obstacles, &allobstacles, &av_vels, &fnl_av_vels, &sendbuf, &recvbuf, &sizes, &displs, rank, nprocs);

  const float viscosity = 1.f / 6.f * (2.f / params.omega - 1.f);

  if (rank == MASTER) {
    /* Init time stops here, compute time starts*/
    gettimeofday(&timstr, NULL);
    init_toc = timstr.tv_sec + (timstr.tv_usec / 1000000.0);
    comp_tic = init_toc;
  }

  for (int tt = 0; tt < params.maxIters; tt++) //params.maxIters
  {
    accelerate_flow(&params, &cells, obstacles);
    haloex(&params, &cells, sendbuf, recvbuf, &status);
    av_vels[tt] = rebound(&params, &cells, &tmp_cells, obstacles);
    /* Pointer swap */
    intermediate = cells;
    cells = tmp_cells;
    tmp_cells = intermediate;

  /*partialdensity = total_density(params, cells);
    MPI_Reduce(&partialdensity, &totaldensity, params.nprocs, MPI_FLOAT, MPI_SUM, MASTER, MPI_COMM_WORLD);
    printf("==timestep: %d==\n", tt);
    printf("tot density: %.12E\n", totaldensity); */
  #ifdef DEBUG
    partialdensity = total_density(&params, &cells);
    MPI_Reduce(&partialdensity, &totaldensity, params.nprocs, MPI_FLOAT, MPI_SUM, MASTER, MPI_COMM_WORLD);
    printf("==timestep: %d==\n", tt);
    printf("av velocity: %.12E\n", av_vels[tt]);
    printf("tot density: %.12E\n", totaldensity);
  #endif
  }

  if (rank == MASTER) {
    /* Compute time stops here, collate time starts*/
    gettimeofday(&timstr, NULL);
    comp_toc = timstr.tv_sec + (timstr.tv_usec / 1000000.0);
    col_tic  = comp_toc;

  /*  for (int qw = 0; qw < nprocs; qw++) {
      printf("sizes: %d\n", sizes[qw]);
      printf("displs: %d\n", displs[qw]);
      printf("elts: %d\n", (params.nx * (params.arraysize - 2)));
    } */
  }

  MPI_Reduce(av_vels, fnl_av_vels, params.maxIters, MPI_FLOAT, MPI_SUM, MASTER, MPI_COMM_WORLD); // reduces all average velocities to the master thread. THIS MAY HAVE TO BE DONE INSIDE THE COMPUTE TIMING SECTION, ASK!

  // use MPI_Gatherv here to gather all results into fnl_cells.
  MPI_Gatherv(&cells.speeds0[params.nx], (params.nx * (params.arraysize - 2)), MPI_FLOAT, fnl_cells.speeds0, sizes, displs, MPI_FLOAT, MASTER, MPI_COMM_WORLD);
  MPI_Gatherv(&cells.speeds1[params.nx], (params.nx * (params.arraysize - 2)), MPI_FLOAT, fnl_cells.speeds1, sizes, displs, MPI_FLOAT, MASTER, MPI_COMM_WORLD);
  MPI_Gatherv(&cells.speeds2[params.nx], (params.nx * (params.arraysize - 2)), MPI_FLOAT, fnl_cells.speeds2, sizes, displs, MPI_FLOAT, MASTER, MPI_COMM_WORLD);
  MPI_Gatherv(&cells.speeds3[params.nx], (params.nx * (params.arraysize - 2)), MPI_FLOAT, fnl_cells.speeds3, sizes, displs, MPI_FLOAT, MASTER, MPI_COMM_WORLD);
  MPI_Gatherv(&cells.speeds4[params.nx], (params.nx * (params.arraysize - 2)), MPI_FLOAT, fnl_cells.speeds4, sizes, displs, MPI_FLOAT, MASTER, MPI_COMM_WORLD);
  MPI_Gatherv(&cells.speeds5[params.nx], (params.nx * (params.arraysize - 2)), MPI_FLOAT, fnl_cells.speeds5, sizes, displs, MPI_FLOAT, MASTER, MPI_COMM_WORLD);
  MPI_Gatherv(&cells.speeds6[params.nx], (params.nx * (params.arraysize - 2)), MPI_FLOAT, fnl_cells.speeds6, sizes, displs, MPI_FLOAT, MASTER, MPI_COMM_WORLD);
  MPI_Gatherv(&cells.speeds7[params.nx], (params.nx * (params.arraysize - 2)), MPI_FLOAT, fnl_cells.speeds7, sizes, displs, MPI_FLOAT, MASTER, MPI_COMM_WORLD);
  MPI_Gatherv(&cells.speeds8[params.nx], (params.nx * (params.arraysize - 2)), MPI_FLOAT, fnl_cells.speeds8, sizes, displs, MPI_FLOAT, MASTER, MPI_COMM_WORLD);

  if (rank == MASTER) {
    for (int rr = 0; rr < params.maxIters; rr++) {
      fnl_av_vels[rr] /= params.tot_cells;
    }
    /* Total/collate time stops here.*/
    gettimeofday(&timstr, NULL);
    col_toc = timstr.tv_sec + (timstr.tv_usec / 1000000.0);
    tot_toc = col_toc;

    /* write final values and free memory */
    printf("==done==\n");
    printf("Reynolds number:\t\t%.12E\n", (fnl_av_vels[params.maxIters - 1] * params.reynolds_dim / viscosity));
    printf("Elapsed Init time:\t\t\t%.6lf (s)\n",    init_toc - init_tic);
    printf("Elapsed Compute time:\t\t\t%.6lf (s)\n", comp_toc - comp_tic);
    printf("Elapsed Collate time:\t\t\t%.6lf (s)\n", col_toc  - col_tic);
    printf("Elapsed Total time:\t\t\t%.6lf (s)\n",   tot_toc  - tot_tic);
    write_values(&params, &fnl_cells, allobstacles, fnl_av_vels);
  }

  finalise(&params, &cells, &tmp_cells, &fnl_cells, &obstacles, &allobstacles, &av_vels, &fnl_av_vels, &sendbuf, &recvbuf, &sizes, &displs);

  /* finalise the MPI environment */
  MPI_Finalize();

  return EXIT_SUCCESS;
}

int accelerate_flow(const t_param* params_ptr, t_speed* cells_ptr, char* restrict obstacles_ptr)
{
  __assume_aligned(cells_ptr->speeds0, 64);
  __assume_aligned(cells_ptr->speeds1, 64);
  __assume_aligned(cells_ptr->speeds2, 64);
  __assume_aligned(cells_ptr->speeds3, 64);
  __assume_aligned(cells_ptr->speeds4, 64);
  __assume_aligned(cells_ptr->speeds5, 64);
  __assume_aligned(cells_ptr->speeds6, 64);
  __assume_aligned(cells_ptr->speeds7, 64);
  __assume_aligned(cells_ptr->speeds8, 64);

  /* modify the 2nd row of the grid */
  int jj = params_ptr->ny - 2;
  if (jj >= params_ptr->offset && jj < params_ptr->nextoffset) {
    if (params_ptr->offset) {
      jj %= params_ptr->offset;
    }
    jj++; // skip halo line
    /* compute weighting factors */
    const float w1 = params_ptr->density * params_ptr->accel / 9.f;
    const float w2 = params_ptr->density * params_ptr->accel / 36.f;

    for (int ii = 0; ii < params_ptr->nx; ii++)
    {
      /* if the cell is not occupied and
      ** we don't send a negative density */
      if (!obstacles_ptr[ii + (jj*params_ptr->nx)]
          && (cells_ptr->speeds3[ii + (jj*params_ptr->nx)] - w1) > 0.f
          && (cells_ptr->speeds6[ii + (jj*params_ptr->nx)] - w2) > 0.f
          && (cells_ptr->speeds7[ii + (jj*params_ptr->nx)] - w2) > 0.f)
      {
        /* increase 'east-side' densities */
        cells_ptr->speeds1[ii + (jj*params_ptr->nx)] += w1;
        cells_ptr->speeds5[ii + (jj*params_ptr->nx)] += w2;
        cells_ptr->speeds8[ii + (jj*params_ptr->nx)] += w2;
        /* decrease 'west-side' densities */
        cells_ptr->speeds3[ii + (jj*params_ptr->nx)] -= w1;
        cells_ptr->speeds6[ii + (jj*params_ptr->nx)] -= w2;
        cells_ptr->speeds7[ii + (jj*params_ptr->nx)] -= w2;
      }
    }
  }
  return EXIT_SUCCESS;
}

int haloex(const t_param* params_ptr, t_speed* cells_ptr, float* sendbuf, float* recvbuf, MPI_Status* status)
{
  int x; /* Generic loop counter */

  __assume_aligned(cells_ptr->speeds0, 64);
  __assume_aligned(cells_ptr->speeds1, 64);
  __assume_aligned(cells_ptr->speeds2, 64);
  __assume_aligned(cells_ptr->speeds3, 64);
  __assume_aligned(cells_ptr->speeds4, 64);
  __assume_aligned(cells_ptr->speeds5, 64);
  __assume_aligned(cells_ptr->speeds6, 64);
  __assume_aligned(cells_ptr->speeds7, 64);
  __assume_aligned(cells_ptr->speeds8, 64);
  __assume((params_ptr->nx)%2==0);
  __assume((params_ptr->nx)%4==0);
  __assume((params_ptr->nx)%8==0);
  __assume((params_ptr->nx)%16==0);
  __assume((params_ptr->nx)%32==0);
  __assume((params_ptr->nx)%64==0);
  __assume((params_ptr->nx)%128==0);
  // fill the sendbuffer with the first non halo row of the cells grid.
  for (x = 0; x < params_ptr->nx; x++) {
    sendbuf[(x * NSPEEDS)]     = cells_ptr->speeds0[x + params_ptr->nx];
    sendbuf[(x * NSPEEDS + 1)] = cells_ptr->speeds1[x + params_ptr->nx];
    sendbuf[(x * NSPEEDS + 2)] = cells_ptr->speeds2[x + params_ptr->nx];
    sendbuf[(x * NSPEEDS + 3)] = cells_ptr->speeds3[x + params_ptr->nx];
    sendbuf[(x * NSPEEDS + 4)] = cells_ptr->speeds4[x + params_ptr->nx];
    sendbuf[(x * NSPEEDS + 5)] = cells_ptr->speeds5[x + params_ptr->nx];
    sendbuf[(x * NSPEEDS + 6)] = cells_ptr->speeds6[x + params_ptr->nx];
    sendbuf[(x * NSPEEDS + 7)] = cells_ptr->speeds7[x + params_ptr->nx];
    sendbuf[(x * NSPEEDS + 8)] = cells_ptr->speeds8[x + params_ptr->nx];
  }

  //send to the prevrank and recv from the nextrank.
  MPI_Sendrecv(sendbuf, (params_ptr->nx * NSPEEDS), MPI_FLOAT, params_ptr->prevrank, params_ptr->rank, recvbuf, (params_ptr->nx * NSPEEDS), MPI_FLOAT, params_ptr->nextrank, params_ptr->nextrank, MPI_COMM_WORLD, status);

  //unpack the recvbuf into the speeds arrays.
  for (x = 0; x < params_ptr->nx; x++) {
    cells_ptr->speeds0[x + ((params_ptr->arraysize - 1) * params_ptr->nx)] = recvbuf[(x * NSPEEDS)];
    cells_ptr->speeds1[x + ((params_ptr->arraysize - 1) * params_ptr->nx)] = recvbuf[(x * NSPEEDS) + 1];
    cells_ptr->speeds2[x + ((params_ptr->arraysize - 1) * params_ptr->nx)] = recvbuf[(x * NSPEEDS) + 2];
    cells_ptr->speeds3[x + ((params_ptr->arraysize - 1) * params_ptr->nx)] = recvbuf[(x * NSPEEDS) + 3];
    cells_ptr->speeds4[x + ((params_ptr->arraysize - 1) * params_ptr->nx)] = recvbuf[(x * NSPEEDS) + 4];
    cells_ptr->speeds5[x + ((params_ptr->arraysize - 1) * params_ptr->nx)] = recvbuf[(x * NSPEEDS) + 5];
    cells_ptr->speeds6[x + ((params_ptr->arraysize - 1) * params_ptr->nx)] = recvbuf[(x * NSPEEDS) + 6];
    cells_ptr->speeds7[x + ((params_ptr->arraysize - 1) * params_ptr->nx)] = recvbuf[(x * NSPEEDS) + 7];
    cells_ptr->speeds8[x + ((params_ptr->arraysize - 1) * params_ptr->nx)] = recvbuf[(x * NSPEEDS) + 8];
    // pack the sendbuf again.
    sendbuf[(x * NSPEEDS)]     = cells_ptr->speeds0[x + ((params_ptr->arraysize - 2) * params_ptr->nx)];
    sendbuf[(x * NSPEEDS) + 1] = cells_ptr->speeds1[x + ((params_ptr->arraysize - 2) * params_ptr->nx)];
    sendbuf[(x * NSPEEDS) + 2] = cells_ptr->speeds2[x + ((params_ptr->arraysize - 2) * params_ptr->nx)];
    sendbuf[(x * NSPEEDS) + 3] = cells_ptr->speeds3[x + ((params_ptr->arraysize - 2) * params_ptr->nx)];
    sendbuf[(x * NSPEEDS) + 4] = cells_ptr->speeds4[x + ((params_ptr->arraysize - 2) * params_ptr->nx)];
    sendbuf[(x * NSPEEDS) + 5] = cells_ptr->speeds5[x + ((params_ptr->arraysize - 2) * params_ptr->nx)];
    sendbuf[(x * NSPEEDS) + 6] = cells_ptr->speeds6[x + ((params_ptr->arraysize - 2) * params_ptr->nx)];
    sendbuf[(x * NSPEEDS) + 7] = cells_ptr->speeds7[x + ((params_ptr->arraysize - 2) * params_ptr->nx)];
    sendbuf[(x * NSPEEDS) + 8] = cells_ptr->speeds8[x + ((params_ptr->arraysize - 2) * params_ptr->nx)];
  }

  //send to the nextrank and recv from the prevrank.
  MPI_Sendrecv(sendbuf, (params_ptr->nx * NSPEEDS), MPI_FLOAT, params_ptr->nextrank, params_ptr->rank, recvbuf, (params_ptr->nx * NSPEEDS), MPI_FLOAT, params_ptr->prevrank, params_ptr->prevrank, MPI_COMM_WORLD, status);

  for (x = 0; x < params_ptr->nx; x++) {
    cells_ptr->speeds0[x] = recvbuf[(x * NSPEEDS)];
    cells_ptr->speeds1[x] = recvbuf[(x * NSPEEDS) + 1];
    cells_ptr->speeds2[x] = recvbuf[(x * NSPEEDS) + 2];
    cells_ptr->speeds3[x] = recvbuf[(x * NSPEEDS) + 3];
    cells_ptr->speeds4[x] = recvbuf[(x * NSPEEDS) + 4];
    cells_ptr->speeds5[x] = recvbuf[(x * NSPEEDS) + 5];
    cells_ptr->speeds6[x] = recvbuf[(x * NSPEEDS) + 6];
    cells_ptr->speeds7[x] = recvbuf[(x * NSPEEDS) + 7];
    cells_ptr->speeds8[x] = recvbuf[(x * NSPEEDS) + 8];
  }
  return EXIT_SUCCESS;
}

float rebound(const t_param* restrict params_ptr, const t_speed* restrict cells_ptr, t_speed* restrict tmp_cells_ptr, const char* restrict obstacles_ptr)
{
  const float c_sq = 1.f / 3.f;  /* square of speed of sound */
  const float w0   = 4.f / 9.f;  /* weighting factor */
  const float w1   = 1.f / 9.f;  /* weighting factor */
  const float w2   = 1.f / 36.f; /* weighting factor */
  const float precomb = 1 / (2.f * c_sq * c_sq);

  float tot_u = 0.f;    /* accumulated magnitudes of velocity for each cell */

  float * restrict cellsspeed0 = cells_ptr->speeds0;
  float * restrict cellsspeed1 = cells_ptr->speeds1;
  float * restrict cellsspeed2 = cells_ptr->speeds2;
  float * restrict cellsspeed3 = cells_ptr->speeds3;
  float * restrict cellsspeed4 = cells_ptr->speeds4;
  float * restrict cellsspeed5 = cells_ptr->speeds5;
  float * restrict cellsspeed6 = cells_ptr->speeds6;
  float * restrict cellsspeed7 = cells_ptr->speeds7;
  float * restrict cellsspeed8 = cells_ptr->speeds8;

  float * restrict tmpcellsspeed0 = tmp_cells_ptr->speeds0;
  float * restrict tmpcellsspeed1 = tmp_cells_ptr->speeds1;
  float * restrict tmpcellsspeed2 = tmp_cells_ptr->speeds2;
  float * restrict tmpcellsspeed3 = tmp_cells_ptr->speeds3;
  float * restrict tmpcellsspeed4 = tmp_cells_ptr->speeds4;
  float * restrict tmpcellsspeed5 = tmp_cells_ptr->speeds5;
  float * restrict tmpcellsspeed6 = tmp_cells_ptr->speeds6;
  float * restrict tmpcellsspeed7 = tmp_cells_ptr->speeds7;
  float * restrict tmpcellsspeed8 = tmp_cells_ptr->speeds8;

  __assume_aligned(tmpcellsspeed0, 64);
  __assume_aligned(tmpcellsspeed1, 64);
  __assume_aligned(tmpcellsspeed2, 64);
  __assume_aligned(tmpcellsspeed3, 64);
  __assume_aligned(tmpcellsspeed4, 64);
  __assume_aligned(tmpcellsspeed5, 64);
  __assume_aligned(tmpcellsspeed6, 64);
  __assume_aligned(tmpcellsspeed7, 64);
  __assume_aligned(tmpcellsspeed8, 64);
  __assume_aligned(cellsspeed0, 64);
  __assume_aligned(cellsspeed1, 64);
  __assume_aligned(cellsspeed2, 64);
  __assume_aligned(cellsspeed3, 64);
  __assume_aligned(cellsspeed4, 64);
  __assume_aligned(cellsspeed5, 64);
  __assume_aligned(cellsspeed6, 64);
  __assume_aligned(cellsspeed7, 64);
  __assume_aligned(cellsspeed8, 64);

  /* loop over the non halo cells in the grid */
  for (int jj = 1; jj < params_ptr->nya; jj++)
  {
    /* determine indices of axis-direction neighbours
    ** respecting periodic boundary conditions (wrap around) */
    const int y_n = (jj + 1);
    const int y_s = (jj - 1);
    float local_tot_u = 0.f;

    for (int ii = 0; ii < params_ptr->nx; ii++)
    {
      __assume((params_ptr->nx)%4==0);
      __assume((params_ptr->nx)%8==0);
      __assume((params_ptr->nx)%16==0);
      __assume((params_ptr->nx)%32==0);
      __assume((params_ptr->nx)%64==0);
      /* determine indices of axis-direction neighbours
      ** respecting periodic boundary conditions (wrap around) */
      const int x_e = (ii + 1) % params_ptr->nx;
      const int x_w = (ii == 0) ? (params_ptr->nx - 1) : (ii - 1);

      const float speed0 = cellsspeed0[ii + (jj*params_ptr->nx)]; /* central cell, no movement */
      const float speed1 = cellsspeed1[x_w + (jj*params_ptr->nx)]; /* east x_w + (jj*params_ptr->nx) */
      const float speed2 = cellsspeed2[ii + (y_s*params_ptr->nx)]; /* north ii + y_s*params_ptr->nx */
      const float speed3 = cellsspeed3[x_e + (jj*params_ptr->nx)]; /* west x_e + (jj*params_ptr->nx) */
      const float speed4 = cellsspeed4[ii + (y_n*params_ptr->nx)]; /* south ii + y_n*params_ptr->nx */
      const float speed5 = cellsspeed5[x_w + (y_s*params_ptr->nx)]; /* north-east x_w + y_s*params_ptr->nx */
      const float speed6 = cellsspeed6[x_e + (y_s*params_ptr->nx)]; /* north-west x_e + y_s*params_ptr->nx */
      const float speed7 = cellsspeed7[x_e + (y_n*params_ptr->nx)]; /* south-west x_e + y_n*params_ptr->nx */
      const float speed8 = cellsspeed8[x_w + (y_n*params_ptr->nx)]; /* south-east x_w + y_n*params_ptr->nx */

      float u_sq = 0.f;
      /* if the cell contains an obstacle */
      if (obstacles_ptr[ii + (jj*params_ptr->nx)])
      {
        /* Propagates and rebounds in one step, putting the result into tmp_cells */
        tmpcellsspeed0[ii + (jj*params_ptr->nx)] = speed0;
        tmpcellsspeed1[ii + (jj*params_ptr->nx)] = speed3;
        tmpcellsspeed2[ii + (jj*params_ptr->nx)] = speed4;
        tmpcellsspeed3[ii + (jj*params_ptr->nx)] = speed1;
        tmpcellsspeed4[ii + (jj*params_ptr->nx)] = speed2;
        tmpcellsspeed5[ii + (jj*params_ptr->nx)] = speed7;
        tmpcellsspeed6[ii + (jj*params_ptr->nx)] = speed8;
        tmpcellsspeed7[ii + (jj*params_ptr->nx)] = speed5;
        tmpcellsspeed8[ii + (jj*params_ptr->nx)] = speed6;
      } else
      {
        /* propagate densities from neighbouring cells, following
        ** appropriate directions of travel and writing into
        ** scratch space grid */

        /* compute local density total */
        const float local_density = speed0 + speed1 + speed2
                                  + speed3 + speed4 + speed5
                                  + speed6 + speed7 + speed8;

        /* compute x velocity component */
        const float u_x = (speed1 + speed5 + speed8
                        - (speed3 + speed6 + speed7))
                        / local_density;
        /* compute y velocity component */
        const float u_y = (speed2 + speed5 + speed6
                        - (speed4 + speed7 + speed8))
                        / local_density;

        /* velocity squared */
        u_sq = (u_x * u_x) + (u_y * u_y);

        /* directional velocity components */
        const float u_0 =   u_x;        /* east */
        const float u_1 =   u_y;        /* north */
        const float u_2 =   u_x + u_y;  /* north-east */
        const float u_3 = - u_x + u_y;  /* north-west */

        /* Variables to help precompute things */
        const float precoma = 1 - ((u_sq) / (2 * c_sq));

        /* zero velocity density: weight w0 */
        const float d_equ_0 = w0 * local_density * (1.f - u_sq / (2.f * c_sq));
        /* axis speeds: weight w1 */
        const float d_equ_1 = w1 * local_density * (precoma + u_0 / c_sq + (u_0 * u_0) * precomb);
        const float d_equ_2 = w1 * local_density * (precoma + u_1 / c_sq + (u_1 * u_1) * precomb);
        const float d_equ_3 = w1 * local_density * (precoma - u_0 / c_sq + (u_0 * u_0) * precomb);
        const float d_equ_4 = w1 * local_density * (precoma - u_1 / c_sq + (u_1 * u_1) * precomb);
        /* diagonal speeds: weight w2 */
        const float d_equ_5 = w2 * local_density * (precoma + u_2 / c_sq + (u_2 * u_2) * precomb);
        const float d_equ_6 = w2 * local_density * (precoma + u_3 / c_sq + (u_3 * u_3) * precomb);
        const float d_equ_7 = w2 * local_density * (precoma - u_2 / c_sq + (u_2 * u_2) * precomb);
        const float d_equ_8 = w2 * local_density * (precoma - u_3 / c_sq + (u_3 * u_3) * precomb);

        /* relaxation step */
        tmpcellsspeed0[ii + (jj*params_ptr->nx)] = speed0 + params_ptr->omega *(d_equ_0 - speed0);
        tmpcellsspeed1[ii + (jj*params_ptr->nx)] = speed1 + params_ptr->omega *(d_equ_1 - speed1);
        tmpcellsspeed2[ii + (jj*params_ptr->nx)] = speed2 + params_ptr->omega *(d_equ_2 - speed2);
        tmpcellsspeed3[ii + (jj*params_ptr->nx)] = speed3 + params_ptr->omega *(d_equ_3 - speed3);
        tmpcellsspeed4[ii + (jj*params_ptr->nx)] = speed4 + params_ptr->omega *(d_equ_4 - speed4);
        tmpcellsspeed5[ii + (jj*params_ptr->nx)] = speed5 + params_ptr->omega *(d_equ_5 - speed5);
        tmpcellsspeed6[ii + (jj*params_ptr->nx)] = speed6 + params_ptr->omega *(d_equ_6 - speed6);
        tmpcellsspeed7[ii + (jj*params_ptr->nx)] = speed7 + params_ptr->omega *(d_equ_7 - speed7);
        tmpcellsspeed8[ii + (jj*params_ptr->nx)] = speed8 + params_ptr->omega *(d_equ_8 - speed8);
      }
      local_tot_u += sqrtf(u_sq);
    }
    tot_u += local_tot_u;
  }
  return tot_u;
}

int initialise(const char* paramfile, const char* obstaclefile,
               t_param* params, t_speed* cells_ptr, t_speed* tmp_cells_ptr, t_speed* fnl_cells_ptr,
               char** obstacles_ptr, char** all_obstacles_ptr, float** av_vels_ptr, float** fnl_av_vels_ptr, float** sendbuf, float** recvbuf, int** sizes_ptr, int** displs_ptr, const int rank, const int nprocs)
{
  char   message[1024]; /* message buffer */
  FILE*  fp;            /* file pointer */
  int    xx, yy, r;     /* generic array indices */
  int    blocked;       /* indicates whether a cell is blocked by an obstacle */
  int    retval;        /* to hold return value for checking */
  int    counter = 0;   /* to assist vectorisation and remove rounding bugs earlier */

  /* open the parameter file */
  fp = fopen(paramfile, "r");

  if (fp == NULL)
  {
    sprintf(message, "could not open input parameter file: %s", paramfile);
    die(message, __LINE__, __FILE__);
  }

  /* read in the parameter values */
  retval = fscanf(fp, "%d\n", &(params->nx));

  if (retval != 1) die("could not read param file: nx", __LINE__, __FILE__);

  retval = fscanf(fp, "%d\n", &(params->ny));

  if (retval != 1) die("could not read param file: ny", __LINE__, __FILE__);

  retval = fscanf(fp, "%d\n", &(params->maxIters));

  if (retval != 1) die("could not read param file: maxIters", __LINE__, __FILE__);

  retval = fscanf(fp, "%d\n", &(params->reynolds_dim));

  if (retval != 1) die("could not read param file: reynolds_dim", __LINE__, __FILE__);

  retval = fscanf(fp, "%f\n", &(params->density));

  if (retval != 1) die("could not read param file: density", __LINE__, __FILE__);

  retval = fscanf(fp, "%f\n", &(params->accel));

  if (retval != 1) die("could not read param file: accel", __LINE__, __FILE__);

  retval = fscanf(fp, "%f\n", &(params->omega));

  if (retval != 1) die("could not read param file: omega", __LINE__, __FILE__);

  /* and close up the file */
  fclose(fp);

  /*
  ** Allocate memory.
  **
  ** Remember C is pass-by-value, so we need to
  ** pass pointers into the initialise function.
  **
  ** NB we are allocating a 1D array, so that the
  ** memory will be contiguous.  We still want to
  ** index this memory as if it were a (row major
  ** ordered) 2D array, however.  We will perform
  ** some arithmetic using the row and column
  ** coordinates, inside the square brackets, when
  ** we want to access elements of this array.
  **
  ** Note also that we are using a structure to
  ** hold an array of 'speeds'.  We will allocate
  ** a 1D array of these structs.
  */
  params->rank = rank;
  params->nprocs = nprocs;
  params->nextrank = (rank + 1) % nprocs;

  if (!rank) {
    params->prevrank = nprocs - 1;
  }
  else {
    params->prevrank = rank - 1;
  }

  params->remainder = params->ny % nprocs;
  counter = params->ny / nprocs;

  if (rank < params->remainder) {
    params->arraysize = counter + 3; // we add an extra row for some ranks in order to make sure that the whole grid is as equally distributed as possible.
    params->offset = (rank * counter) + rank;
    params->nextoffset = ((rank + 1) * counter) + rank + 1;
  }
  else {
    params->arraysize = (counter) + 2;
    params->offset = (rank * counter) + params->remainder;
    params->nextoffset = ((rank + 1) * counter) + params->remainder;
  }

//  if (*cells_ptr == NULL) die("cannot allocate memory for cells", __LINE__, __FILE__);
  /* main grid */
  cells_ptr->speeds0 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  cells_ptr->speeds1 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  cells_ptr->speeds2 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  cells_ptr->speeds3 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  cells_ptr->speeds4 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  cells_ptr->speeds5 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  cells_ptr->speeds6 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  cells_ptr->speeds7 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  cells_ptr->speeds8 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);

//  if (*tmp_cells_ptr == NULL) die("cannot allocate memory for tmp_cells", __LINE__, __FILE__);
  /* 'helper' grid, used as scratch space */
  tmp_cells_ptr->speeds0 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  tmp_cells_ptr->speeds1 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  tmp_cells_ptr->speeds2 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  tmp_cells_ptr->speeds3 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  tmp_cells_ptr->speeds4 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  tmp_cells_ptr->speeds5 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  tmp_cells_ptr->speeds6 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  tmp_cells_ptr->speeds7 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);
  tmp_cells_ptr->speeds8 = _mm_malloc(sizeof(float) * (params->arraysize * params->nx), 64);

  if (rank == MASTER) {
    //  if (*fnl_cells_ptr == NULL) die("cannot allocate memory for tmp_cells", __LINE__, __FILE__);
    /* 'helper' grid, used as scratch space */
    fnl_cells_ptr->speeds0 = _mm_malloc(sizeof(float) * (params->ny * params->nx), 64);
    fnl_cells_ptr->speeds1 = _mm_malloc(sizeof(float) * (params->ny * params->nx), 64);
    fnl_cells_ptr->speeds2 = _mm_malloc(sizeof(float) * (params->ny * params->nx), 64);
    fnl_cells_ptr->speeds3 = _mm_malloc(sizeof(float) * (params->ny * params->nx), 64);
    fnl_cells_ptr->speeds4 = _mm_malloc(sizeof(float) * (params->ny * params->nx), 64);
    fnl_cells_ptr->speeds5 = _mm_malloc(sizeof(float) * (params->ny * params->nx), 64);
    fnl_cells_ptr->speeds6 = _mm_malloc(sizeof(float) * (params->ny * params->nx), 64);
    fnl_cells_ptr->speeds7 = _mm_malloc(sizeof(float) * (params->ny * params->nx), 64);
    fnl_cells_ptr->speeds8 = _mm_malloc(sizeof(float) * (params->ny * params->nx), 64);
    *all_obstacles_ptr = _mm_malloc(sizeof(char) * (params->ny * params->nx), 64);
  }

  /* the map of obstacles */
  *obstacles_ptr = _mm_malloc(sizeof(char) * (params->arraysize * params->nx), 64);

// if (*obstacles_ptr == NULL) die("cannot allocate column memory for obstacles", __LINE__, __FILE__);

  /* initialise densities */
  const float w0 = params->density * 4.f / 9.f;
  const float w1 = params->density       / 9.f;
  const float w2 = params->density      / 36.f;

  __assume_aligned((*cells_ptr).speeds0, 64);
  __assume_aligned((*cells_ptr).speeds1, 64);
  __assume_aligned((*cells_ptr).speeds2, 64);
  __assume_aligned((*cells_ptr).speeds3, 64);
  __assume_aligned((*cells_ptr).speeds4, 64);
  __assume_aligned((*cells_ptr).speeds5, 64);
  __assume_aligned((*cells_ptr).speeds6, 64);
  __assume_aligned((*cells_ptr).speeds7, 64);
  __assume_aligned((*cells_ptr).speeds8, 64);
  __assume_aligned(obstacles_ptr, 64);
  __assume((params->nx)%2==0);
  __assume((params->nx)%4==0);
  __assume((params->nx)%8==0);
  __assume((params->nx)%16==0);
  __assume((params->nx)%32==0);
  __assume((params->nx)%64==0);
  __assume((params->nx)%128==0);

  for (int jj = 1; jj < params->arraysize - 1; jj++)
  {
    for (int ii = 0; ii < params->nx; ii++)
    {
      /* centre */
      (*cells_ptr).speeds0[ii + jj*params->nx] = w0;
      /* axis directions */
      (*cells_ptr).speeds1[ii + jj*params->nx] = w1;
      (*cells_ptr).speeds2[ii + jj*params->nx] = w1;
      (*cells_ptr).speeds3[ii + jj*params->nx] = w1;
      (*cells_ptr).speeds4[ii + jj*params->nx] = w1;
      /* diagonals */
      (*cells_ptr).speeds5[ii + jj*params->nx] = w2;
      (*cells_ptr).speeds6[ii + jj*params->nx] = w2;
      (*cells_ptr).speeds7[ii + jj*params->nx] = w2;
      (*cells_ptr).speeds8[ii + jj*params->nx] = w2;
      /* first set all cells in obstacle array to zero */
      (*obstacles_ptr)[ii + jj*params->nx] = 0;
    }
  }
  /* first set all cells in obstacle array to zero */
  if (rank == MASTER) {
    for (int jj = 1; jj < params->ny; jj++) {
      for (int ii = 0; ii < params->nx; ii++) {
        (*all_obstacles_ptr)[ii + jj*params->nx] = 0;
      }
    }
  }

  params->tot_cells = params->nx * params->ny;

  /* open the obstacle data file */
  fp = fopen(obstaclefile, "r");

  if (fp == NULL)
  {
    sprintf(message, "could not open input obstacles file: %s", obstaclefile);
    die(message, __LINE__, __FILE__);
  }

  /* read-in the blocked cells list */
  while ((retval = fscanf(fp, "%d %d %d\n", &xx, &yy, &blocked)) != EOF)
  {
    /* some checks */
    if (retval != 3) die("expected 3 values per line in obstacle file", __LINE__, __FILE__);

    if (xx < 0 || xx > params->nx - 1) die("obstacle x-coord out of range", __LINE__, __FILE__);

    if (yy < 0 || yy > params->ny - 1) die("obstacle y-coord out of range", __LINE__, __FILE__);

    if (blocked != 1) die("obstacle blocked value should be 1", __LINE__, __FILE__);

    /* assign to array */
    if (rank == MASTER) {
      (*all_obstacles_ptr)[xx + yy*params->nx] = blocked;
    }
    if (yy >= params->offset && yy < params->nextoffset) {
      if (params->offset) {
        yy %= params->offset;
      }
      yy++;
      (*obstacles_ptr)[xx + yy*params->nx] = blocked;
    }
    params->tot_cells--;
  }
  /* and close the file */
  fclose(fp);

  params->nya = params->arraysize - 1;

  /*
  ** allocate buffers the width of a row of the grid to allow haloex.
  */
  *sendbuf = (float*)_mm_malloc(sizeof(float) * params->nx * NSPEEDS, 64);
  *recvbuf = (float*)_mm_malloc(sizeof(float) * params->nx * NSPEEDS, 64);

  /*
  ** allocate spaces to hold records of the average velocities computed
  ** at each timestep
  */
  *av_vels_ptr = (float*)_mm_malloc(sizeof(float) * params->maxIters, 64);
  if (rank == MASTER) {
    *fnl_av_vels_ptr = (float*)_mm_malloc(sizeof(float) * params->maxIters, 64);
    /*
    ** allocate sizes array and populate.
    */
    *sizes_ptr = (int*)_mm_malloc(sizeof(int) * nprocs, 64);
    *displs_ptr = (int*)_mm_malloc(sizeof(int) * nprocs, 64);

    for (r = 0; r < nprocs; r++) {
      (*sizes_ptr)[r] = (params->ny / nprocs);
    }

    for (r = 0; r < params->remainder; r++) {
      (*sizes_ptr)[r]++;
    }

    (*displs_ptr)[0] = 0;
    counter = 0;

    for (r = 1; r < nprocs; r++) {
      (*displs_ptr)[r] = counter + (*sizes_ptr)[(r - 1)];
      counter = (*displs_ptr)[r];
    }

    for (r = 0; r < nprocs; r++) {
      (*displs_ptr)[r] *= params->nx;
      (*sizes_ptr)[r] *= params->nx;
    }
  }

  /*if (rank == MASTER) {
    printf("nextrank %d\n", params->nextrank);
    printf("prevrank %d\n", params->prevrank);
    printf("remainder %d\n", params->remainder);
    printf("arraysize %d\n", params->arraysize);
    printf("offset %d\n", params->offset);
    printf("nextoffset %d\n", params->nextoffset);
    printf("nya %d\n", params->nya);
  } */

  return EXIT_SUCCESS;
}

int finalise(const t_param* params_ptr, t_speed* cells_ptr, t_speed* tmp_cells_ptr, t_speed* fnl_cells_ptr,
             char** obstacles_ptr, char** all_obstacles_ptr, float** av_vels_ptr, float** fnl_av_vels_ptr, float** sendbuf, float** recvbuf, int** sizes_ptr, int** displs_ptr)
{
  /*
  ** free up allocated memory and pointers in the struct
  */
  _mm_free(cells_ptr->speeds0);
  _mm_free(cells_ptr->speeds1);
  _mm_free(cells_ptr->speeds2);
  _mm_free(cells_ptr->speeds3);
  _mm_free(cells_ptr->speeds4);
  _mm_free(cells_ptr->speeds5);
  _mm_free(cells_ptr->speeds6);
  _mm_free(cells_ptr->speeds7);
  _mm_free(cells_ptr->speeds8);

  _mm_free(tmp_cells_ptr->speeds0);
  _mm_free(tmp_cells_ptr->speeds1);
  _mm_free(tmp_cells_ptr->speeds2);
  _mm_free(tmp_cells_ptr->speeds3);
  _mm_free(tmp_cells_ptr->speeds4);
  _mm_free(tmp_cells_ptr->speeds5);
  _mm_free(tmp_cells_ptr->speeds6);
  _mm_free(tmp_cells_ptr->speeds7);
  _mm_free(tmp_cells_ptr->speeds8);

  _mm_free(*obstacles_ptr);
  *obstacles_ptr = NULL;

  _mm_free(*av_vels_ptr);
  *av_vels_ptr = NULL;

  _mm_free(*sendbuf);
  *sendbuf = NULL;

  _mm_free(*recvbuf);
  *recvbuf = NULL;

  if (params_ptr->rank == MASTER) {
    _mm_free(*fnl_av_vels_ptr);
    _mm_free(fnl_cells_ptr->speeds0);
    _mm_free(fnl_cells_ptr->speeds1);
    _mm_free(fnl_cells_ptr->speeds2);
    _mm_free(fnl_cells_ptr->speeds3);
    _mm_free(fnl_cells_ptr->speeds4);
    _mm_free(fnl_cells_ptr->speeds5);
    _mm_free(fnl_cells_ptr->speeds6);
    _mm_free(fnl_cells_ptr->speeds7);
    _mm_free(fnl_cells_ptr->speeds8);
    _mm_free(*sizes_ptr);
    _mm_free(*displs_ptr);
    _mm_free(*all_obstacles_ptr);
  }
  *fnl_av_vels_ptr = NULL;
  *all_obstacles_ptr = NULL;
  *sizes_ptr = NULL;
  *displs_ptr = NULL;

  return EXIT_SUCCESS;
}

float total_density(const t_param* params_ptr, const t_speed* cells_ptr)
{
  float total = 0.f;  /* accumulator */

  __assume_aligned(cells_ptr->speeds0, 64);
  __assume_aligned(cells_ptr->speeds1, 64);
  __assume_aligned(cells_ptr->speeds2, 64);
  __assume_aligned(cells_ptr->speeds3, 64);
  __assume_aligned(cells_ptr->speeds4, 64);
  __assume_aligned(cells_ptr->speeds5, 64);
  __assume_aligned(cells_ptr->speeds6, 64);
  __assume_aligned(cells_ptr->speeds7, 64);
  __assume_aligned(cells_ptr->speeds8, 64);
  __assume((params_ptr->nx)%2==0);
  __assume((params_ptr->nx)%4==0);
  __assume((params_ptr->nx)%8==0);
  __assume((params_ptr->nx)%16==0);
  __assume((params_ptr->nx)%32==0);
  __assume((params_ptr->nx)%64==0);
  __assume((params_ptr->nx)%128==0);

  for (int jj = 1; jj < params_ptr->nya; jj++)
  {
    for (int ii = 0; ii < params_ptr->nx; ii++)
    {
      total += cells_ptr->speeds0[ii + (jj*params_ptr->nx)] + cells_ptr->speeds1[ii + (jj*params_ptr->nx)] + cells_ptr->speeds2[ii + (jj*params_ptr->nx)]
            + cells_ptr->speeds3[ii + (jj*params_ptr->nx)] + cells_ptr->speeds4[ii + (jj*params_ptr->nx)] + cells_ptr->speeds5[ii + (jj*params_ptr->nx)]
            + cells_ptr->speeds6[ii + (jj*params_ptr->nx)] + cells_ptr->speeds7[ii + (jj*params_ptr->nx)] + cells_ptr->speeds8[ii + (jj*params_ptr->nx)];
    }
  }

  return total;
}

int write_values(const t_param* params_ptr, const t_speed* cells_ptr, const char* obstacles_ptr, const float* av_vels_ptr)
{
  FILE* fp;                     /* file pointer */
  const float c_sq = 1.f / 3.f; /* sq. of speed of sound */
  float local_density;         /* per grid cell sum of densities */
  float pressure;              /* fluid pressure in grid cell */
  float u_x;                   /* x-component of velocity in grid cell */
  float u_y;                   /* y-component of velocity in grid cell */
  float u;                     /* norm--root of summed squares--of u_x and u_y */

  fp = fopen(FINALSTATEFILE, "w");

  if (fp == NULL)
  {
    die("could not open file output file", __LINE__, __FILE__);
  }

  __assume_aligned(cells_ptr->speeds0, 64);
  __assume_aligned(cells_ptr->speeds1, 64);
  __assume_aligned(cells_ptr->speeds2, 64);
  __assume_aligned(cells_ptr->speeds3, 64);
  __assume_aligned(cells_ptr->speeds4, 64);
  __assume_aligned(cells_ptr->speeds5, 64);
  __assume_aligned(cells_ptr->speeds6, 64);
  __assume_aligned(cells_ptr->speeds7, 64);
  __assume_aligned(cells_ptr->speeds8, 64);
  __assume((params_ptr->nx)%2==0);
  __assume((params_ptr->nx)%4==0);
  __assume((params_ptr->nx)%8==0);
  __assume((params_ptr->nx)%16==0);
  __assume((params_ptr->nx)%32==0);
  __assume((params_ptr->nx)%64==0);
  __assume((params_ptr->nx)%128==0);

  for (int jj = 0; jj < params_ptr->ny; jj++)
  {
    for (int ii = 0; ii < params_ptr->nx; ii++)
    {
      /* an occupied cell */
      if (obstacles_ptr[ii + (jj*params_ptr->nx)])
      {
        u_x = u_y = u = 0.f;
        pressure = params_ptr->density * c_sq;
      }
      /* no obstacle */
      else
      {
        /* local density total */
        float local_density = 0.f;

        local_density = cells_ptr->speeds0[ii + (jj*params_ptr->nx)] + cells_ptr->speeds1[ii + (jj*params_ptr->nx)] + cells_ptr->speeds2[ii + (jj*params_ptr->nx)]
                      + cells_ptr->speeds3[ii + (jj*params_ptr->nx)] + cells_ptr->speeds4[ii + (jj*params_ptr->nx)] + cells_ptr->speeds5[ii + (jj*params_ptr->nx)]
                      + cells_ptr->speeds6[ii + (jj*params_ptr->nx)] + cells_ptr->speeds7[ii + (jj*params_ptr->nx)] + cells_ptr->speeds8[ii + (jj*params_ptr->nx)];

        /* compute x velocity component */
        float u_x = (cells_ptr->speeds1[ii + (jj*params_ptr->nx)]
                    + cells_ptr->speeds5[ii + (jj*params_ptr->nx)]
                    + cells_ptr->speeds8[ii + (jj*params_ptr->nx)]
                    - (cells_ptr->speeds3[ii + (jj*params_ptr->nx)]
                    + cells_ptr->speeds6[ii + (jj*params_ptr->nx)]
                    + cells_ptr->speeds7[ii + (jj*params_ptr->nx)]))
                    / local_density;
        /* compute y velocity component */
        float u_y = (cells_ptr->speeds2[ii + (jj*params_ptr->nx)]
                    + cells_ptr->speeds5[ii + (jj*params_ptr->nx)]
                    + cells_ptr->speeds6[ii + (jj*params_ptr->nx)]
                    - (cells_ptr->speeds4[ii + (jj*params_ptr->nx)]
                    + cells_ptr->speeds7[ii + (jj*params_ptr->nx)]
                    + cells_ptr->speeds8[ii + (jj*params_ptr->nx)]))
                    / local_density;
        /* compute norm of velocity */
        u = sqrtf((u_x * u_x) + (u_y * u_y));
        /* compute pressure */
        pressure = local_density * c_sq;
      }

      /* write to file */
      fprintf(fp, "%d %d %.12E %.12E %.12E %.12E %d\n", ii, jj, u_x, u_y, u, pressure, obstacles_ptr[(jj) * params_ptr->nx + ii]);
    }
  }

  fclose(fp);

  fp = fopen(AVVELSFILE, "w");

  if (fp == NULL)
  {
    die("could not open file output file", __LINE__, __FILE__);
  }

  for (int ii = 0; ii < params_ptr->maxIters; ii++)
  {
    fprintf(fp, "%d:\t%.12E\n", ii, av_vels_ptr[ii]);
  }

  fclose(fp);

  return EXIT_SUCCESS;
}

void die(const char* message, const int line, const char* file)
{
  fprintf(stderr, "Error at line %d of file %s:\n", line, file);
  fprintf(stderr, "%s\n", message);
  fflush(stderr);
  exit(EXIT_FAILURE);
}

void usage(const char* exe)
{
  fprintf(stderr, "Usage: %s <paramfile> <obstaclefile>\n", exe);
  exit(EXIT_FAILURE);
}
