\documentclass[a4paper,10pt,twocolumn]{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
%you can add more packages using the same code above

%------------------

%\setlength{\topmargin}{0.0in}
%\setlength{\textheight}{10in}
%\setlength{\oddsidemargin}{0.0in}
%\setlength{\evensidemargin}{0.0in}
%\setlength{\textwidth}{5.5in}

%-------------------
\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem*{remark}{Remark}

\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem*{example}{Example}

%------------------

%Everything before begin document is called the pre-amble and sets out how the document will look
%It is recommended you don't touch the pre-amble until you are familiar with LateX

\begin{document}
	
\title{Parallelising Lattice Boltzmann - a HPC project}
\author{Arun W. Steward - cw18903}
\date{February 21st 2021}
\maketitle

\begin{abstract}
	In this report we will explore the methods used in the parallelisation of the given C serial implementation of the d2q9-bgk lattice Boltzmann scheme and explore and analyse the effects of these methods. Please note that a major family bereavement struck during the period of working on this project causing a complete loss of 18 days of the coursework window.
\end{abstract}

\section{Serial optimistions}

In this section we look at some of the first optimisations done to the serial code.

\subsection{Compilers}

The first changes that were made were in Makefile and the file env.sh. Firstly a module for a new version of GCC was added (GCC 9.1.0). This is worthwhile because the new version of the compiler suite will generate faster programs than the old compiler suite. This is thanks to ``more optimised code-generating algorithms" \cite{GCC}.

The next change to be made was to optimise the compiler flags used by the Makefile to invoke the compiler suite. This involved:
\begin{itemize}
		\item Changing the optimisation level flag from the default \emph{-O2} to \emph{-Ofast}. This tells the compiler to more aggressively optimise the code it is compiling, as detailed in lectures.
		\item Adding the new compiler flag \emph{-mtune=native}. This tells the compiler to ``perform optimizations for specific processors" \cite{Intel}, in this case the native tells it to optimise for the processor being used at the time, as this code is compiled on Blue Crystal phase 4, where all processors are the same type (Intel E5-2680 v4 (Broadwell) CPU), this flag will tell the compiler to optimise for the same processor found in the compute nodes.
\end{itemize}

The next step is to experiment with other compiler suites than GCC. After some experimentation with several versions of ICC, the Intel compiler suite, we decide that the 2018 version be loaded in env.sh and used. 

\subsection{Profiler \& loop fusion}

At this point, we enable the Gprof profiler using the flag \emph{-pg} in the Makefile. The profiler tells us that there are three functions called: \emph{propagate}, \emph{rebound} and \emph{collision} in the C file which together account for 89\% of the total run-time of the program. Upon inspection of the C code, we discover that these are sequentially called functions.  As these four functions take the vast majority of the execution time of the program, by the quote from David Knuth given in lectures, we should focus on optimising them.

In order to make the most from the parallelisation later on, we need to fuse the \emph{propagate}, \emph{rebound} and \emph{collision} kernels. We start this by merging the \emph{rebound} and \emph{collision} kernels. The result is a combined kernel that will perform computation regardless of if there is an obstacle or not. This will aid in producing a more balanced workload later when this kernel is made parallel with OpenMP. Having done this, we now move on to merging the \emph{propagate} kernel into the combined kernel we have just made. However, before we can do this, we should note that there is a lot of very expensive and unnecessary copying of data between the two arrays \emph{cells} and \emph{tmp\_cells}. It should be noted that because the lattice Boltzmann scheme is almost always memory bandwidth bound, this is especially bad for performance. Therefore, we eliminate this copying from \emph{cells} to \emph{tmp\_cells} and then back to \emph{cells}. Changing the scheme so that the input to these three kernels is the \emph{cells} array, and we leave the output in the \emph{tmp\_cells} array and perform a pointer swap afterwards which is far less expensive. Now we merge the \emph{propagate} kernel with the combined kernel to give a single kernel which is more efficient.

\subsection{Misc. serial optimisations}

Now we are able to do some other simple simplifications to the code. They are as follows:
\begin{enumerate}
		\item Combining the propagation and rebound steps inside our combined kernel into one set of operations in the case that there is an obstacle to reduce the number of FLOPS needed.
		\item Reducing memory usage by changing the \emph{obstacles} array from an array of size ``(sizeof(int) * (params.ny * params.nx))" to an array of size ``(sizeof(char) * (params.ny * params.nx))". This reduces the size of the \emph{obstacles} array in memory to one quarter of its original size as an int is usually 32 bits and a char is usually 8.
		\item Other small changes including avoiding unnecessary re-computation of values as well as pre-computing some commonly used values and finally eliminating the \emph{timestep} function entirely.
\end{enumerate}

Before we finish serial optimisation, we can make one final change which drastically improves the performance of the program. On the Intel compiler website we find a page on an ICC compiler flag, \emph{-march=native}. When added to the Makefile in place of  \emph{-mtune=native}, this compiler flag will tell ICC to use features and instruction sets specific to the specified processor which will allow the program to fully exploit the power of the processors it is being run on. This new compiler flag added to the Makefile results in a big change which makes our optimised serial program considerably faster than the ballpark timing.

\section{OpenMP parallelism}

Due to time pressure, we now skip to making the program parallel using OpenMP. To do this we begin adding OpenMP pragma directives to a number of \emph{for} loops. Most importantly, the combined \emph{rebound} kernel and the \emph{av\_velocity} function, which the profiler still says account for the vast majority of the program runtime between them. Where necessary we add some reductions for accumulator variables such as in the \emph{av\_velocity} function.

\section{Vectorisation}

In this section we will look at the process of vectorising the parallel program. There is one place of particular note in the C code that can be improved by vectorisation. This place is the still expensive, combined kernel we made in the first section, which we made parallel using OpenMP in the last section. As we have parallelised the outer loop of this kernel using OpenMP, we cannot vectorise this loop. However, we can vectorise the inner loop and expect to see some performance gains. More specifically, we will help the compiler vectorise this inner loop for us.

\subsection{AOS to SOA}

Firstly, we need to see why our compiler is not vectorising our inner loop so that we can understand what the problem is, and then hopefully fix it. Using the Intel Advisor \cite{Intel} we can generate a vectorisation report using the flag \emph{-qopt-report=5} which we add to the Makefile. At this stage, we are told that one of the reasons for not vectorising the loop is ``memory access with a stride length of 9". The problem causing this is that we have an array of structures (AOS) for the two arrays \emph{cells} and \emph{tmp\_cells}. This causes the data field which we want to access from each array element to be separated in memory from the corresponding fields in other array elements. The solution to this is that we must convert the entire program to use the structure of arrays (SOA) layout. Now for example, the zero-th speed variables for each cell are contiguous in memory and therefore vectorisation is worthwhile!

\subsection{Memory alignment}

Before we are done with vectorising, we have one last change to make. When we declare the arrays inside our structures, there is no guarantee of alignment of the array elements with with any boundary. This can lead to needing two cache lines to cache one variable, which is a waste of memory bandwidth. We solve this by switching from using the functions \emph{malloc} and \emph{free} to using \emph{\_\_mm\_\_malloc} and \emph{\_\_mm\_\_free}. These take an extra argument which must be a power of two and guarantee that the pointer returned or freed is aligned on the specified boundary \cite{mm}.

\section{Analysis}

The following results were gathered by running the different versions of software described in this report on Blue Crystal phase 4 using several numbers of cores and the $1024 \times 1024$ benchmark. The data from bench-marking the original serial code is given to provide a baseline for comparison and also to allow speedup calculations. Please note that due to time constraints, for some unknown reason, the program is slower than the ballpark, the fact that it has vectorised is confirmed by the Intel Advisor. All runtimes are averages over 30 runs so that, by the central limit theorem, the numbers quoted are representative of the true runtime.

\subsection{Runtimes}

%runtime table
\begin{table}[h]
\begin{center}
\tiny
 \begin{tabular}{||c c c||} 
 \hline
 Program Version & Wall time (s) & Compounding speedup \\ [0.5ex] 
 \hline\hline
 Original serial & 1241.3 & 1 \\ 
 \hline
 Optimised serial & 501.3 & 2.476 \\
 \hline
 OpenMP (28 threads) & 124.0 & 10.011 \\
 \hline
 Final (1 thread) & 510.8 & 2.430 \\
 \hline
 Final (2 threads) & 254.6 & 4.875 \\
 \hline
 Final (4 threads) & 118.0 & 10.516 \\
 \hline
 Final (8 threads) & 61.8 & 20.091 \\
 \hline
 Final (16 threads) & 44.1 & 28.176 \\
 \hline
 Final (28 threads) & 35.9 & 34.589 \\
 \hline
\end{tabular}
\end{center}
\caption{The runtime and speedup values for all code versions, including different core counts.}
\label{runtimes}
\end{table}

\begin{figure}[h]
   \centering
    \includegraphics[scale=0.6]{speedup.png}
    \caption{Plot of the speedup values.}
    \label{speedup}
\end{figure}

\begin{figure}[h]
   \centering
    \includegraphics[scale=0.6]{cores.png}
    \caption{Plot wall time execution against the core count used.}
    \label{cores}
\end{figure}

%analysis of scaling and explanation
In figure \ref{speedup}, we can see that the program has a huge speedup overall. It is interesting to note that when looking at the scaling of the program's performance with doubling numbers of cores, we can see that the speedup factor over the base program initially is scaling linearly (even better when changing from 4 to 8 threads). After a certain point however, this trend stops and the performance differential begins to tail off (although is still positive). This suggests that at higher numbers of threads, say 32, we might see a sublinear plateau in performance. A likely reason for this is that only some of the code is parallel and so not all of it will benefit from more cores. Amdahl's law then predicts that a sublinear plateau will occur.

\subsection{Roofline analysis}

The following is a handmade roofline analysis of the vectorised and parallel \emph{rebound} kernel, taken by counting loads, stores and floating point operations for the $1024 \times 1024$ example. The processor in question is the Intel E5-2680 v4 (Broadwell) CPU as found in Blue Crystal phase 4.

%roofline analysis figure
\begin{figure}[h]
   \centering
    \includegraphics[scale=0.225]{roofline.png}
    \caption{The roofline model for the vectorised OpenMP lattice Boltzmann program generated from running the $1024 \times1024$ example on the Intel E5-2680 v4 (Broadwell) CPU. All specs sourced from Intel ARK \cite{Intel ARK}.}
    \label{roofline}
\end{figure}

%anaylsis of roofline and explanation.
In figure \ref{roofline}, we can see that only the peak memory bandwidth for the DRAM is included. The reason for this is that the size of the data structures used in the program with the $1024 \times 1024$ example is approximately equal to $x := 2 \times 1024^2 \times 9 \times 4 = 75497472$ bytes. A simple breakdown of this formula is that there are two sets of cells. Each of these sets is of size $1024 \times 1024$. Each element of these sets is a set of nine single precision floating point numbers, each of which is 4 bytes. We already know, by inspecting the Intel ARK page \cite{Intel ARK} on our CPU in question, that the size of the L3 cache is 35MB. Therefore, a single node of Blue Crystal phase 4 contains $2 \times 35 = 70$MB of L3 cache. It then follows that we cannot fit all of the data required into L3 cache and so the data must instead be stored in DRAM, hence this is the memory bandwidth bound in our case.

On the graph in figure \ref{roofline}, we can see that our program we have made sits on the graph as a dot. Its x and y coordinates on the graph indicate the operational intensity and single precision GFLOPS/s (SIMD) respectively. As we can see, this dot lies very far to the left of the ridge point. This means that our program is very memory bound, as all lattice Boltzmann programs are doomed to be. More alarming however is that our program is very far below the peak theoretical DRAM bandwidth, in fact it is only approximately 15\% of the value. This means that the program is performing well below its potential even as is. Something which is probably happening, and contributing to this problem, is that we cannot ensure memory affinity between the two CPUs in the compute node. Most CPUs today, including the Intel E5-2680 v4 (Broadwell) CPU include a memory controller on the same silicon with the cores. If the system has two multicore CPUs, then some addresses go to the DRAM local to one multicore CPU and the rest must pass through the CPU interconnect to access the DRAM that is local to the other CPU. The latter case reduces performance significantly. Because every cell must draw data from its neighbours, there will be cases where this phenomena happens. Which will become the rate limiting factor for the timestep. A solution to this problem would be to prefetch the data from the other CPU in these tricky cases.

Another way to improve the program would be to try and move the dot on the graph in figure \ref{roofline} to the right. This would raise the performance ceiling and allow us to run the program faster. A potential improvement that could be made is to switch the program to using 16 bit, half precision floating point numbers, and using bit packing to store them inside 32 bit unsigned int or 64 bit unsigned long numbers. This would result in a slight decrease in accuracy but it would also reduce the number of loads and stores done by the program by almost 50\%, increasing the operational intensity and moving our dot further to the right.

\section{Conclusion}

In conclusion, this report and analysis has shown that as real world computing problems get larger and larger, the role of HPC and the disciplines such as vectorisation or parallelisation will have to grow too in order to service these demands efficiently. Even a toy problem like the small size lattice Boltzmann program we have explored here demonstrates the ability of such techniques to have huge impact on the runtime of computations thus causing great improvements to energy usage and therefore environmental footprint. With the current trends in hardware and our needs to compute large problems, these optimisations will only continue to grow in importance.

%\begin{figure}
%   \centering
%    \includegraphics[scale=0.3]{logo-full-colour.png}
%    \caption{The logo for the University of Bristol}
%    \label{fig:logo}
%\end{figure}


\begin{thebibliography}{99}
\tiny
\bibitem{GCC}
	GCC - the GNU Compiler Collection,
    [Online],
    Accessed at https://gcc.gnu.org/,
    (February 27th 2021).

\bibitem{Intel}
	Developer Guide and Reference,
    [Online],
    Accessed at https://software.intel.com/content/www/us/en/-develop/documentation/cpp-compiler-developer-guide-and-reference/top/compiler-reference/compiler-options/compiler-option-details/code-generation-options/mtune-tune.html,
    (February 27th 2021).

\bibitem{Intel ARK}
	Intel Ark,
    [Online],
    Accessed at https://ark.intel.com/content/www/us/en/ark/-products/91754/intel-xeon-processor-e5-2680-v4-35m-cache-2-40-ghz.html,
    (February 27th 2021).

\bibitem{mm}
	Allocating and Freeing Aligned Memory Blocks,
    [Online],
    Accessed at https://scc.ustc.edu.cn/zlsc/chinagrid/intel/compiler\_c/-main\_cls/GUID-D0927A8E-A220-4F50-8697-C89BBE6EFC95.htm,
    (February 28th 2021).

\end{thebibliography}

\end{document}